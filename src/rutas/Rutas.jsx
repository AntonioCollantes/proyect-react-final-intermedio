import { BrowserRouter as Router, Switch } from 'react-router-dom';
import RutaPublica from './RutaPublica';
import RutaPrivada from './RutaPrivada';
import Login from '../View/Login';
import React, { useEffect, useState, useContext } from 'react';
import MyContextAuth from '../contexAuth/ContexAuth';
import Alumnos from '../View/Alumnos';
import AddAlumno from '../View/Mantenimiento/AddAlumno';
import EditAlumno from '../View/Mantenimiento/EditAlumno';
import PagePrincipal from '../View/PagePrincipal';
import CursosView from '../View/Cursos/CursosView';
import DetalleCurso from '../View/Cursos/DetalleCurso';
import FormCrearCurso from '../View/Cursos/AgregarCurso';

const Rutas = () => {

    const [estasAutenticado, setEstasAutenticado] = useState(false);
    const [consultado, setConsultando] = useState(true);

    const { estaAutenticado } = useContext(MyContextAuth);

    useEffect(() => {
        if (estaAutenticado) {
            setEstasAutenticado(true);
        } else {
            setEstasAutenticado(false);
        }
        setConsultando(false);
    }, []);

    if (consultado) return <h1 className='text-center'>Cargando .....</h1>;

    return (
        <Router>
            <Switch>
                <RutaPublica
                    estasAutenticado={estaAutenticado}
                    exact
                    path='/'
                    component={Login}
                />
                <RutaPrivada
                    estasAutenticado={estaAutenticado}
                    exact
                    path='/principal'
                    component={PagePrincipal}
                />
                <RutaPrivada
                    estasAutenticado={estaAutenticado}
                    exact
                    path='/alumno'
                    component={Alumnos}
                />
                <RutaPrivada
                    estasAutenticado={estaAutenticado}
                    exact
                    path='/alumno/nuevo'
                    component={AddAlumno}
                />
                <RutaPrivada
                    estasAutenticado={estaAutenticado}
                    exact
                    path='/alumno/editar/:id'
                    component={EditAlumno}
                />
                <RutaPrivada
                    estasAutenticado={estaAutenticado}
                    exact
                    path='/alumno/detalle/:id'
                    component={Alumnos}
                />
                <RutaPrivada
                    estasAutenticado={estaAutenticado}
                    exact
                    path='/curso'
                    component={CursosView}
                />
                <RutaPrivada
                    estasAutenticado={estaAutenticado}
                    exact
                    path='/curso/nuevo'
                    component={FormCrearCurso}
                />
                <RutaPrivada
                    estasAutenticado={estaAutenticado}
                    exact
                    path='/curso/detalle/:id'
                    component={DetalleCurso}
                />
            </Switch>
        </Router>
    );
};

export default Rutas;