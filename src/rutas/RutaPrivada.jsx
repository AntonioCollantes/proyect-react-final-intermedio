import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Menu from '../Menu/Menu';

const RutaPrivada = ({ estasAutenticado, component: Component, ...rest }) => {
  return (
    <>
      <Menu />
      <main className='container'>
        <Route
          {...rest}
          component={(props) =>
            estasAutenticado ? <Component {...props} /> : <Redirect to='/' />
          }
        />
      </main>
    </>
  );
};

export default RutaPrivada;
