import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const RutaPublica = ({ estasAutenticado, component: Component, ...rest }) => {
  return (
    <>
      <Route
        {...rest}
        component={() =>
          estasAutenticado ? <Redirect to='/principal' /> : <Component />
        }
      />
    </>
  );
};

export default RutaPublica;
