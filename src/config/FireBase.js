import firebase from 'firebase/compat';
import 'firebase/compat/firestore'
import 'firebase/compat/auth'

const firebaseConfig = {
    apiKey: "AIzaSyC2PEmxRSbM4a1D393wwxzxBtXpNf_0Fhg",
    authDomain: "universidad-antonio-collantes.firebaseapp.com",
    projectId: "universidad-antonio-collantes",
    storageBucket: "universidad-antonio-collantes.appspot.com",
    messagingSenderId: "309708652729",
    appId: "1:309708652729:web:59e9aa7091155c3b991b68",
    measurementId: "G-HQYEJD9507"
};

const fire = firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

export {fire as default, db} 