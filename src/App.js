import './App.css';
import Rutas from './rutas/Rutas';
import StateAuth from './contexAuth/StateAuth';
import AlumnoState from './ContexAlumno/AlumnoState';

function App() {
  return (
    <StateAuth>
      <AlumnoState>
        <Rutas />
      </AlumnoState>
    </StateAuth>
  );
}

export default App;
