import React, { useReducer } from 'react';
import MyContextAuth from './ContexAuth'
import myReduce from './ReduxeAuth';
import axios from 'axios';
import fire from '../config/FireBase'

const urlApiGetUsuario = "https://61a9072233e9df0017ea3c71.mockapi.io/universidad/usuarios/";
let estaAutenticado = false;

const StateAuth = ({ children }) => {

    const stateInitial = {
        email: '',
        nombres: '',
        login: '',
        error: ''
    }

    const loginSystem = async (email, password) => {
        try {
            axios.get(`${urlApiGetUsuario}/${email}`)
                .then(res => {
                    if (password === res.data.password) {
                        estaAutenticado = true;
                        //Estoy validando el logearme con un API, si es correcto me logeo al FireBase
                        fire.auth().signInWithEmailAndPassword("jantonio6.java@gmail.com", "123456789");
                        dispatch(loginSucces(email, res.data.nombres));
                    } else {
                        estaAutenticado = false;
                        dispatch(loginError());
                    }
                })
        } catch (error) {
            estaAutenticado = false;
            console.log(error);
        }
    }

    const loginError = () => {
        return {
            type: 'NOOK',
            payload: { error: 'Contraseña Incorrecta' }
        }; 
    };

    const loginSucces = (email, nombres) => {
        return {
            type: 'OK',
            payload: { email: email, nombres: nombres, login: 'ok' }
        };
    };

    const cerrarSesion = async () => {
        try {
            estaAutenticado = false;
            await fire.auth().signOut();
            dispatch(logout());
        } catch (error) { }
    };

    const logout = () => {
        return {
            type: 'LOGOUT',
        };
    };

    const [state, dispatch] = useReducer(myReduce, stateInitial);

    return (
        <MyContextAuth.Provider
            value={{ loginSystem, cerrarSesion, estaAutenticado}}
        >
            {children}
        </MyContextAuth.Provider>
    );
};

export default StateAuth;