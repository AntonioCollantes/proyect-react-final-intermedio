const myReduce = (state, accion) => {
    switch (accion.type) {
        case 'OK':
            return {
                ...state,
                email: accion.payload.email,
                nombres: accion.payload.nombres,
                login: accion.payload.login,
                error: ''
            };
        case 'NOOK':
            return {
                email: '',
                nombres: '',
                login: '',
                error: accion.payload.error
            };
        case 'LOGOUT':
            return {
                email: '',
                nombres: '',
                login: '',
                error: ''
            }
        default:
            return state;
    }
};

export default myReduce;