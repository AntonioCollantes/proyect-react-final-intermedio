import { createContext } from "react";

const MyContextAuth = createContext();

export default MyContextAuth;