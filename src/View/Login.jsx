import React, { useContext, useState } from 'react';
import { Form, Button, StyleInput, BannerDiv, Container } from './style';
import MyContextAuth from '../contexAuth/ContexAuth';
import mensaje from './mensaje'

let msjEmailError = "";
let msjPswError = "";

const Login = () => {

    const [formData, setFormData] = useState({
        email: '',
        password: ''
    });

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const { loginSystem } = useContext(MyContextAuth);

    const handleSubmit = (e) => {
        e.preventDefault();
        const { email, password } = formData;
        if (validarFormulario(email, password)) {
            loginSystem(email, password);
        } else {
            mensaje("Error al iniciar session:\n" + msjEmailError + "\n" + msjPswError, 'warning');
        }
    };

    const validarFormulario = (email, password) => {
        let campoEmail = email;
        let campoPassword = password;
        let formularioValido = true;

        msjEmailError = "";
        msjPswError = "";

        if (!campoEmail) {
            formularioValido = false;
            console.log("dentro de " + !campoEmail);
            msjEmailError = "Por favor, ingresa tu correo.";
        } else {
            if (typeof campoEmail !== "undefined") {
                let posicionArroba = campoEmail.lastIndexOf('@');
                let posicionPunto = campoEmail.lastIndexOf('.');
                if (!(posicionArroba < posicionPunto && posicionArroba > 0 && campoEmail.indexOf('@@') == -1 && posicionPunto > 2 && (campoEmail.length - posicionPunto) > 2)) {
                    formularioValido = false;
                    msjEmailError = "Por favor, ingresa un correo válido.";
                }
            }
        }

        if (!campoPassword) {
            formularioValido = false;
            msjPswError = "Por favor, ingresa tu contraseña.";
        }

        return formularioValido;
    }

    return (
        <Container>
            <BannerDiv />
            <div className="row justify-content-center align-items-center">
                <div className="col-md-6">
                    <div className="col-md-12">
                        <Form onSubmit={handleSubmit}>
                            <h3 className="text-center text-info">Login</h3>
                            <div className="form-group">
                                <label htmlFor="username" className="text-info">Username:</label><br />
                                <StyleInput
                                    name='email'
                                    type='email'
                                    onChange={handleChange}
                                    value={formData.email}
                                    placeholder="Ingrese Email"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password" className="text-info">Password:</label><br />
                                <StyleInput
                                    type='password'
                                    name='password'
                                    placeholder='Ingresar Contraseña'
                                    onChange={handleChange}
                                    value={formData.password}
                                />
                            </div>
                            <br />
                            <div className="form-group">
                                <Button type='submit'>Login</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        </Container>
    );
};

export default Login;