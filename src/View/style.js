import styled, { css } from 'styled-components';

export const Form = styled.form`
  padding: 20px;
`;

const sharedInputStyled = css`
  display: block;
  width: 100%;
  background-color: white;
  border-radius: 5px;
  border: 1px solid #ddd;
  margin: 10px 0 20px 0;
  padding: 20px;
  box-sizing: border-box;
`;

export const StyleInput = styled.input`
  border: 1px solid black;
  background: yellow;
  height: 40px;
  ${sharedInputStyled}
`;

export const Button = styled.button`
  background: blue;
  color:white;
  font-size: 1em;
  margin: 1em;
  padding: right;
  border: 3px solid black;
  border-radius: 3px;
`;

export const BannerDiv = styled.div`
  color: white;
  background-image: url("https://i1.wp.com/politicalprecinct.xyz/wp-content/uploads/2020/06/login-banner.jpg?resize=1536%2C471&ssl=1");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 30vh;
`;

const sharedStyled = css`
    margin-top: 120px;
    max-width: 600px;
    height: 280;
    border: 3px solid red;
    background-color: #9a8c8c;
`;

export const Container = styled.div`
  max-width: 1140px;
  margin: 0 auto;
  ${sharedStyled}
`;

export const DivTabla = styled.div`
background-color: #71bad3b3;
margin-top: 39px;
`;

export const containerT = styled.div`
  width: 590px;
  margin: 140px auto;
  position: relative;
  `;