import axios from 'axios'

const URL = "https://61a9072233e9df0017ea3c71.mockapi.io/universidad/producto"

const listarCursos = async () => {
    try {
        let { data } = await axios.get(URL)
        return data
    }
    catch (error) {
        return error
    }
}

const buscarCursoId = async (id) => {

    try {
        let { data } = await axios.get(`${URL}/${id}`)
        return data;
    }
    catch (error) {
        return error
    }
}

const registrarCurso = async (objProducto) => {
    try {
        let headers = {
            "Content-Type": "application/json"
        }

        let { data } = await axios.post(URL, objProducto, { headers })
        return data
    }
    catch (error) {
        return error
    }
}

const editarCurso = async (objProducto, id) => {
    try {
        let headers = {
            "Content-Type": "application/json"
        }

        let { data } = await axios.put(`${URL}/${id}`, objProducto, { headers })
        return data

    }
    catch (error) {
        return error
    }
}

const eliminarProducto = async (id) => {
    try {
        let headers = {
            "Content-Type": "application/json"
        }
        let { data } = await axios.delete(`${URL}/${id}`, { headers })
        return data
    } catch (error) {
        return error
    }
}

const subirArchivo = (imagen, refStorage) => {
    return new Promise((resolve, reject) => {
        const tarea = refStorage.put(imagen)
        tarea.on(
            'state_changed',
            () => { },
            (error) => { reject(error) },
            () => {
                tarea.snapshot.ref.getDownloadURL().then(urlImagen => resolve(urlImagen))
            }
        )
    })
}

export {
    listarCursos,
    buscarCursoId,
    registrarCurso,
    editarCurso,
    eliminarProducto,
    subirArchivo
}