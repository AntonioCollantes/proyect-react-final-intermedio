import React, { useState, useEffect, useContext, Fragment } from 'react'
import { buscarCursoId } from '../Cursos/CursosService/CursoService'
import { useHistory } from 'react-router-dom'

export default function DetalleCurso(props) {

    let history = useHistory();
    const id = props.match.params.id;
    const [curso, setCurso] = useState({});

    const getCurso = async () => {
        let response = await buscarCursoId(id);
        setCurso(response);
    }

    useEffect(() => {
        getCurso();
    }, [])

    const retornar = () => {
        history.push('/curso');
    }

    return (
        <div >
            <h1 style={{ color: 'RED' }}>Detalle de Curso</h1>
            <div className="card_left"
                style={{ backgroundImage: `url(${curso.producto_imagen})` }}
            >
            </div>
            <div className="card_right">
                <h1>{curso.producto_nombre}</h1>
                <div className="card_right__details">
                    <div className="card_right__review">
                        <p>{curso.producto_descripcion}</p>
                        <p>S/ {parseFloat(curso.producto_precio).toFixed(2)}</p>
                    </div>
                </div>
                <div className="botones">
                    <button
                        className=" card_right__button mt-2 leftBoton"
                        onClick={() => { retornar() }}
                    >
                        Listado
                    </button>
                </div>
            </div>
        </div>
    )
}