import React, { useState, useEffect } from 'react';
import './styleAlumno.css'
import { useHistory  } from 'react-router-dom';

const dataFormInit = {
    nombres: '',
    apellidos: '',
    telefono: '',
    correo: '',
    fechanacimiento: '',
    direccion: '',
    pais: '',
    carrera: '',
    sexo: '',
    id: ''
};

const errorInit = {
    ...dataFormInit,
};

const VolverListar = () => {
    let history = useHistory();
    function handleClick() {
        history.push("/alumno");
    }

    return (
        <div>
            <button onClick={handleClick} className="btn btn-info btn-lg btn-responsive">Cancelar</button>
        </div>
    );
}

const MantAlumno = ({
    agregarAlumnoNuevo,
    editarAlumno,
    alumno,
    editar = false,
}) => {

    useEffect(() => {
        if (editar) {
            setData({ ...alumno });
        }
    }, []);

    const [data, setData] = useState(dataFormInit);
    const [error, setError] = useState(errorInit);

    const handleChange = (e) => {
        setData({ ...data, [e.target.name]: e.target.value });
        setError({ ...error, [e.target.name]: '' });
    };

    const validarEmail = (email) => {
        if (email !== "") {
            let posicionArroba = email.lastIndexOf('@');
            let posicionPunto = email.lastIndexOf('.');
            if (!(posicionArroba < posicionPunto && posicionArroba > 0 && email.indexOf('@@') == -1 && posicionPunto > 2 && (email.length - posicionPunto) > 2)) {
                return "Ingrese un correo válido.";
            }
        } else {
            return "Email Requerido";
        }
    }

    const isValid = () => {
        let respuesta = true;

        const misErrores = { ...errorInit };

        for (let key in data) {
            if (key !== 'id') {
                if (key == 'correo') {
                    misErrores[key] = validarEmail(data[key]);
                } else {
                    let dato = data[key];
                    if (dato.length === 0 ||  dato.trim() === '') {
                        misErrores[key] = ' requerido';
                        respuesta = false;
                    }
                }
            }
        }

        setError({ ...misErrores });

        return respuesta;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (isValid()) {
            if (!editar) {
                agregarAlumnoNuevo(data);
            } else {
                editarAlumno(data);
            }
        }
    };

    return (
        <div className="container" id="advanced-search-form">
            <h2>Registrar Alumno</h2>
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="nombres">Nombres</label>
                    <input
                        type="text"
                        name="nombres"
                        className="form-control"
                        placeholder="Nombres"
                        onChange={handleChange}
                        value={data.nombres}
                    />
                    <span style={{ color: 'red', fonSize: '14px' }}>{error.nombres}</span>
                </div>
                <div className="form-group">
                    <label htmlFor="apellidos">Apellidos</label>
                    <input
                        type="text"
                        name="apellidos"
                        className="form-control"
                        placeholder="Apellidos"
                        onChange={handleChange}
                        value={data.apellidos}
                    />
                    <span style={{ color: 'red', fonSize: '14px' }}>{error.apellidos}</span>
                </div>
                <div className="form-group">
                    <label htmlFor="telefono">Telefono</label>
                    <input
                        type="text"
                        name="telefono"
                        className="form-control"
                        placeholder="Telefono"
                        onChange={handleChange}
                        value={data.telefono}
                    />
                    <span style={{ color: 'red', fonSize: '14px' }}>{error.telefono}</span>
                </div>
                <div className="form-group">
                    <label htmlFor="correo">Correo</label>
                    <input
                        type="email"
                        name="correo"
                        className="form-control"
                        placeholder="Email"
                        onChange={handleChange}
                        value={data.correo}
                    />
                    <span style={{ color: 'red', fonSize: '14px' }}>{error.correo}</span>
                </div>
                <div className="form-group">
                    <label htmlFor="fechanacimiento">Fecha Nacimiento</label>
                    <input
                        type="date"
                        name="fechanacimiento"
                        className="form-control"
                        placeholder="Fecha Nacimiento"
                        onChange={handleChange}
                        value={data.fechanacimiento}
                    />
                    <span style={{ color: 'red', fonSize: '14px' }}>{error.fechanacimiento}</span>
                </div>
                <div className="form-group">
                    <label htmlFor="direccion">Direccion</label>
                    <input
                        type="text"
                        name="direccion"
                        className="form-control"
                        placeholder="Direccion"
                        onChange={handleChange}
                        value={data.direccion}
                    />
                    <span style={{ color: 'red', fonSize: '14px' }}>{error.direccion}</span>
                </div>
                <div className="form-group">
                    <label htmlFor="pais">Pais</label>
                    <input
                        type="text"
                        name="pais"
                        className="form-control"
                        placeholder="Pais"
                        onChange={handleChange}
                        value={data.pais}
                    />
                    <span style={{ color: 'red', fonSize: '14px' }}>{error.pais}</span>
                </div>
                <div className="form-group">
                    <label htmlFor="carrera">Carrera</label>
                    <input
                        type="text"
                        name="carrera"
                        className="form-control"
                        placeholder="Carrera"
                        onChange={handleChange}
                        value={data.carrera}
                    />
                    <span style={{ color: 'red', fonSize: '14px' }}>{error.carrera}</span>
                </div>
                <div className="form-group">
                    <label htmlFor="sexo">Genero</label>
                    <div className="radio">
                        <label className="radio-inline">
                            <input
                                type="radio"
                                name="sexo"
                                value="Masculino"
                                onChange={handleChange}
                                checked={data.sexo === 'Masculino'}
                            />
                            Masculino</label>
                        <label className="radio-inline">
                            <input
                                type="radio"
                                name="sexo"
                                value="Femenino"
                                onChange={handleChange}
                                checked={data.sexo === 'Femenino'}
                            />
                            Femenino</label>
                    </div>
                    <span style={{ color: 'red', fonSize: '14px' }}>{error.sexo}</span>
                </div>
                <div className="clearfix"></div>
                <button type="submit" className="btn btn-info btn-lg btn-responsive" id="search">
                    <span className="glyphicon glyphicon-search"></span> 
                    {editar ? 'Modificar' : 'Registrar'}
                </button>
                <VolverListar/>
            </form>
        </div>
    );
};

export default MantAlumno;