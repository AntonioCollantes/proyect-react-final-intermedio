import React, { useContext, useEffect } from 'react';
import MantAlumno from './MantAlumno'
import MyAlumnoContext from '../../ContexAlumno/AlumnoContex';

const AddAlumno = (props) => {

    const { addOk, agregarAlumno } = useContext(MyAlumnoContext);

    useEffect(() => {
        if (addOk) {
            props.history.push('/alumno');
        }
    }, [addOk])

    const addAlumnoNew = (alumno) => {
        agregarAlumno(alumno);
    };

    return (
        <div>
            <h1 style={{ color: 'RED' }}>Agregar Alumno</h1>
            <MantAlumno agregarAlumnoNuevo={addAlumnoNew} />
        </div>
    );
};

export default AddAlumno;