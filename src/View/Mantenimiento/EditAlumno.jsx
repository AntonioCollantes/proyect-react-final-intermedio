import React, { useEffect, useContext } from 'react';
import MyAlumnoContext from '../../ContexAlumno/AlumnoContex';
import MantAlumno from './MantAlumno'

const EditAlumno = (props) => {

    const { editOk, editarAlumno, obtenerAlumno, alumno } = useContext(MyAlumnoContext);

    const existeAlumno = Object.keys(alumno).length;

    useEffect(() => {
        obtenerAlumno(props.match.params.id);
    }, []);

    useEffect(() => {
        if (editOk) {
            props.history.push('/alumno');
        }
    }, [editOk]);

    return (
        <div>
            <h1>Editar Alumno</h1>
            {existeAlumno > 0 && (
                <MantAlumno
                    alumno={alumno}
                    editar={true}
                    editarAlumno={editarAlumno}
                />
            )}
        </div>
    );
};

export default EditAlumno;