import Swal from 'sweetalert2'

const DetalleAlumno = (dataAlumno) => {

    const { nombres, apellidos, telefono, correo, fechanacimiento, direccion, pais, carrera, sexo, id } = dataAlumno;

    Swal.fire({
        title: '<strong>DETALLE ALUMNO</u></strong>',
        icon: 'info',
        html:
        `Id:  ${id}<br/>`+
        `Nombres:  ${nombres}<br/>`+
        `Apellidos:  ${apellidos}<br/>`+
        `Telefono:  ${telefono}<br/>`+
        `Email:  ${correo}<br/>`+
        `F. Nacimiento:  ${fechanacimiento}<br/>`+
        `Pais:  ${pais}<br/>`+
        `Direccion:  ${direccion}<br/>`+
        `Carrera:  ${carrera}<br/>`+
        `Genero:  ${sexo}<br/>`,
        showCloseButton: true
    })

};

export default DetalleAlumno;

