import React from 'react';
import './stylePrincipal.css'

const PagePrincipal = () => {
    return (
        <article className="container">
            <blockquote>
                <strong> FINAL</strong> REACT <em> BÁSICO </em> INTERMEDIO <strong>Projecto Final</strong>
            </blockquote>
            <b>Juan Antonio Collantes Mejia</b>
        </article>
    );
};

export default PagePrincipal;