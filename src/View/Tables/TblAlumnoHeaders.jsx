import React from 'react';
import RowTableAlumno from './RowTableAlumno'

const TblAlumnoHeaders = ({ listaAlumnos }) => {
    return (
        <table className='table'>
            <thead className='thead-dark'>
                <tr>
                    <th scope='col'>#</th>
                    <th scope='col'>Nombres</th>
                    <th scope='col'>Apellidos</th>
                    <th scope='col'>Telefono</th>
                    <th scope='col'>Correo</th>
                    <th scope='col'>Carrera</th>
                </tr>
            </thead>
            <tbody>
                {listaAlumnos.map((item, index) => (
                    <RowTableAlumno key={item.id} info={item} indice={index} />
                ))}
            </tbody>
        </table>
    );
};

export default TblAlumnoHeaders;