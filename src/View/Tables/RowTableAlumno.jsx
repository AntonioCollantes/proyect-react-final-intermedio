import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import MyAlumnoContext from '../../ContexAlumno/AlumnoContex';
import DetalleAlumno from '../Mantenimiento/DetalleAlumno';

const RowTabla = ({ info, indice }) => {
    const { id, apellidos, carrera, correo, nombres, telefono } = info;
  
    const { eliminarAlumno } = useContext(MyAlumnoContext);

    const popup=()=>{
      DetalleAlumno(info);
    }

    return (
      <tr>
        <th scope='row'>{indice + 1}</th>
        <td>{nombres}</td>
        <td>{apellidos}</td>
        <td>{telefono}</td>
        <td>{correo}</td>
        <td>{carrera}</td>
        <td className='d-flex justify-content-around'>
          <Link className='btn btn-warning' to={`/alumno/editar/${id}`}>
            Editar
          </Link>
          <button className='btn btn-danger' onClick={popup}>
            Ver Detalle
          </button>
          <button className='btn btn-danger' onClick={() => eliminarAlumno(id)}>
            Eliminar
          </button>
        </td>
      </tr>
    );
  };
  
  export default RowTabla;