import React, { useContext, useEffect } from 'react';
import TblAlumnoHeaders from './Tables/TblAlumnoHeaders';
import { DivTabla } from './style';
import './StyleAlumnos.css'
import MyAlumnoContext from '../ContexAlumno/AlumnoContex';

const Alumnos = () => {

    const { obtenerAlumnos, listaAlumnos, deleteOk } = useContext(MyAlumnoContext);

    useEffect(() => {
        obtenerAlumnos();
    }, []);

    useEffect(() => {
        if (deleteOk) {
            obtenerAlumnos();
        }
      }, [deleteOk]);

    return (
        <DivTabla>
            <h1>Listado de Alumnos</h1>
            {listaAlumnos.length > 0 && <TblAlumnoHeaders listaAlumnos={listaAlumnos} />}
        </DivTabla>
    );
};

export default Alumnos;