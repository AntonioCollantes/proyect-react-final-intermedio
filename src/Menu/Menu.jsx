import React, { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import ContexAuth from '../contexAuth/ContexAuth';

const Menu = () => {

    const { cerrarSesion } = useContext(ContexAuth);

    const handleClick = (e) => {
        e.preventDefault();
        cerrarSesion();
    };

    return (
        <nav className='navbar navbar-expand-lg navbar-light bg-light'>
            <Link className='navbar-brand' to='/principal'>
                Principal
            </Link>
            <div className='collapse navbar-collapse container-fluid'>
                <ul className='navbar-nav mr-auto mt-2 mt-lg-0'>
                    <li className='nav-item'>
                        <NavLink className='nav-link' to='/alumno'>
                            Alumnos
                        </NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink className='nav-link' to='/alumno/nuevo'>
                            Agregar Alumnos
                        </NavLink>
                    </li>
                </ul>
                <ul className='navbar-nav mr-auto mt-2 mt-lg-0'>
                    <li className='nav-item'>
                        <NavLink className='nav-link' to='/curso'>
                            Cursos
                        </NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink className='nav-link' to='/curso/nuevo'>
                            Agregar Cursos
                        </NavLink>
                    </li>
                </ul>
                <div className='d-flex'>
                    <button onClick={handleClick} className='mr btn btn-warning'>
                        Cerrar Sesión
                    </button>
                </div>
            </div>
        </nav>
    );
};

export default Menu;