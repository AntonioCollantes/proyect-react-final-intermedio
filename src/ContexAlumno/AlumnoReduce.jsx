const reduceAlumno = (state, option) => {
    switch (option.type) {
        case 'ADD_ALUMNO':
            return {
                ...state,
                addOk: option.payload
            };
        case 'LIST_ALUMNOS':
            return {
                ...state,
                addOk: false,
                editOk: false,
                deleteOk: false,
                listaAlumnos: option.payload,
                alumno: {}
            };
        case 'GET_ALUMNO':
            return {
                ...state,
                alumno: option.payload
            };
        case 'EDIT_ALUMNO':
            return {
                ...state,
                editOk: option.payload
            };
        case 'DELETE_ALUMNO':
            return {
                ...state,
                deleteOk: option.payload
            };

        default:
            return state;
    }
}

export default reduceAlumno;