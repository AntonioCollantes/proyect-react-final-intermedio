import React, { useReducer } from 'react';
import MyAlumnoContext from './AlumnoContex';
import reduceAlumno from './AlumnoReduce';
import axios from 'axios';

const apiAlumno = "https://61a9072233e9df0017ea3c71.mockapi.io/universidad/alumnos";

const AlumnoState = ({ children }) => {

    const initialState = {
        listaAlumnos: [],
        alumno: {},
        addOk: false,
        editOk: false,
        deleteOk: false,
    };

    const obtenerAlumnos = async () => {
        try {
            let alumnos = [];
            await axios.get(`${apiAlumno}`).then(
                res => {
                    alumnos = res.data;
                });
            dispatch({
                type: 'LIST_ALUMNOS',
                payload: alumnos,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const obtenerAlumno = async (id) => {
        try {
            let alumno;
            await axios.get(`${apiAlumno}/${id}`).then(
                res => {
                    alumno = res.data;
                });
            dispatch({
                type: 'GET_ALUMNO',
                payload: alumno,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const eliminarAlumno = async (id) => {
        try {

            let headers = {
                "Content-Type": "application/json"
            }

            console.log("Eliminando el Id " + id);
            await axios.delete(`${apiAlumno}/${id}`, { headers });
            dispatch({
                type: 'DELETE_ALUMNO',
                payload: true,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const agregarAlumno = async (alumno) => {
        try {

            let headers = {
                "Content-Type": "application/json"
            }

            await axios.post(`${apiAlumno}`, alumno, { headers });
            dispatch({
                type: 'ADD_ALUMNO',
                payload: true,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const editarAlumno = async (alumno) => {
        try {

            let headers = {
                "Content-Type": "application/json"
            }

            await axios.put(`${apiAlumno}/${alumno.id}`, alumno, { headers });
            dispatch({
                type: 'EDIT_ALUMNO',
                payload: true,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const [state, dispatch] = useReducer(reduceAlumno, initialState);

    return (
        <MyAlumnoContext.Provider
            value={
                {
                    addOk: state.addOk,
                    editOk: state.editOk,
                    deleteOk: state.deleteOk,
                    listaAlumnos: state.listaAlumnos,
                    alumno: state.alumno,
                    agregarAlumno,
                    editarAlumno,
                    eliminarAlumno,
                    obtenerAlumnos,
                    obtenerAlumno
                }
            }
        >
            {children}
        </MyAlumnoContext.Provider>
    );
};

export default AlumnoState;